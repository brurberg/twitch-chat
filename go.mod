module gitlab.com/brurberg/twitch-chat

go 1.15

require (
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/gookit/color v1.3.2
	github.com/tomnomnom/xtermcolor v0.0.0-20160428124646-b78803f00a7e // indirect
)
