package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/gempir/go-twitch-irc/v2"
	"github.com/gookit/color"
)

func main() {
	//client := twitch.NewAnonymousClient() // for an anonymous user (no write capabilities)
	client := twitch.NewClient("dethsanius", "oauth:xbw8g50ddvtasyh6p0w6hnql9q5v0x")

	client.OnPrivateMessage(func(message twitch.PrivateMessage) {
		if _, ok := message.User.Badges["broadcaster"]; ok {
			fmt.Print("[Broadcaster]")
		} else if _, ok := message.User.Badges["moderator"]; ok {
			fmt.Print("[Mod]")
		}
		/*
				if count, ok := message.User.Badges["sub-gifter"]; ok {
					fmt.Printf("[Gifter <%d>]", count)
				}
				if count, ok := message.User.Badges["bits"]; ok {
					fmt.Printf("[Oiler <%d>]", count)
				}
				if _, ok := message.User.Badges["subscriber"]; ok {
					fmt.Print("[SUB]")
				}
			fmt.Print(message.User.Badges)
		*/
		c := color.HEXStyle(message.User.Color) // fg color
		c.Print(message.User.DisplayName)
		fmt.Printf(": %s\n", message.Message)
	})
	client.OnClearChatMessage(func(message twitch.ClearChatMessage) {
		fmt.Println("Chat: ", message.Type, " ", message.Message)
	})
	client.OnClearMessage(func(message twitch.ClearMessage) {
		fmt.Println("CLEAR: ", message.Login, " ", message.Message)
	})
	client.OnGlobalUserStateMessage(func(message twitch.GlobalUserStateMessage) {
		fmt.Println("GLOBAL: ", message)
	})
	client.OnNamesMessage(func(message twitch.NamesMessage) {
		fmt.Println("Names :", message)
	})
	client.OnNoticeMessage(func(message twitch.NoticeMessage) {
		fmt.Println("NOTICE:", message.Message)
	})

	chanell := os.Args[1]
	// chanell := "itsHafu"
	// chanell := "dethsanius"
	client.Join(chanell)

	go func() {
		for {
			reader := bufio.NewReader(os.Stdin)
			//fmt.Print("Enter text: ")
			text, _ := reader.ReadString('\n')
			client.Say(chanell, text)
		}
	}()

	err := client.Connect()
	if err != nil {
		panic(err)
	}

}
